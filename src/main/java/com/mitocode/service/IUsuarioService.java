package com.mitocode.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mitocode.model.Usuario;

public interface IUsuarioService {

	void registrar(Usuario usuario);

	void modificar(Usuario usuario);

	void eliminar(Long idUsuario);

	Usuario listarId(Long idUsuario);

	List<Usuario> listar();
	
	Page<Usuario> listarPageable(Pageable pageable);

}
