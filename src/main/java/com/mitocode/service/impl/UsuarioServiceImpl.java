package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IUsuarioDAO;
import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

	@Autowired
	private IUsuarioDAO dao;

	@Override
	public void registrar(Usuario usuarios) {
		dao.save(usuarios);
		
	}

	@Override
	public void modificar(Usuario usuarios) {
		dao.save(usuarios);
		
	}

	@Override
	public void eliminar(Long idUsuario) {
		dao.delete(idUsuario);
		
	}

	@Override
	public Usuario listarId(Long idUsuario) {
		return dao.findOne(idUsuario);
	}

	@Override
	public List<Usuario> listar() {
		return dao.findAll();
	}

	@Override
	public Page<Usuario> listarPageable(Pageable pageable) {
		return dao.findAll(pageable);
	}
	
	
}
