package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IRolDAO;
import com.mitocode.model.Rol;
import com.mitocode.service.IRolService;

@Service
public class RolServiceImpl implements IRolService {

	@Autowired
	private IRolDAO dao;

	@Override
	public void registrar(Rol rol) {
		dao.save(rol);
	}

	@Override
	public void modificar(Rol rol) {
		dao.save(rol);
	}

	@Override
	public void eliminar(int idRol) {
		dao.delete(idRol);
	}

	@Override
	public Rol listarId(int idRol) {
		return dao.findOne(idRol);
	}

	@Override
	public List<Rol> listar() {
		return dao.findAll();
	}
}
