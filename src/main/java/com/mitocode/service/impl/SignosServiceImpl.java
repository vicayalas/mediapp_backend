package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.ISignosDAO;
import com.mitocode.model.Signos;
import com.mitocode.service.ISignosService;

@Service
public class SignosServiceImpl implements ISignosService {

	@Autowired
	private ISignosDAO dao;

	@Override
	public void registrar(Signos signos) {
		dao.save(signos);
		
	}

	@Override
	public void modificar(Signos signos) {
		dao.save(signos);
		
	}

	@Override
	public void eliminar(int idSignos) {
		dao.delete(idSignos);
		
	}

	@Override
	public Signos listarId(int idSignos) {
		return dao.findOne(idSignos);
	}

	@Override
	public List<Signos> listar() {
		return dao.findAll();
	}
	
	
}
