package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Rol;

public interface IRolService {

	void registrar(Rol rol);

	void modificar(Rol rol);

	void eliminar(int idRol);

	Rol listarId(int idRol);

	List<Rol> listar();

}
